#Stage 0, "build-stage", based on Node.js, to build and compile the frontend
#node:alpine as build-stage --> tiene vulneravilidad, buscar la 18 node.
#FROM node:alpine as build-stage
#WORKDIR /app
#COPY package*.json /app/
#RUN npm install
#COPY ./ /app/
#RUN npm run build

#Stage 1, based on Nginx, to have only the compiled app, ready for production with Nginx

FROM nginx:1.22.1-alpine3.17-slim

COPY ./build/ /usr/share/nginx/html

EXPOSE 80



#--from=build-stage /app/build/ /usr/share/nginx/htm   --> linea 13
